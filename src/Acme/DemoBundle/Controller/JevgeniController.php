<?php

namespace Acme\DemoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Monolog\Logger;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class JevgeniController extends Controller
{
    public function helloAction($name)
    {
        $log = new Logger('name');

        $log->addAlert("OMG!!");
        $log->addError('Bar');

        return $this->render("@AcmeDemo/Jevgeni/hello.html.twig", array('name' => $name));
//        return array('name' => $name);
    }

}
